import pandas as pd
from tabulate import tabulate
import random
import arcade
import math
import os
from PIL import Image, ImageTk
import sys
import pickle
import tkinter as tk
from tkinter import Button, Canvas, Label, Listbox, messagebox
from tkinter.constants import ACTIVE, BROWSE, CENTER, RAISED, RIDGE, SUNKEN

SPRITE_SCALING_PLAYER = 0.4
SPRITE_SCALING_COIN = 0.05
SPRITE_SCALING_LASER = 0.8
SPRITE_SCALING_SNAPE = 0.3
SPRITE_SCALING_BOX = .5
COIN_COUNT = 1
BlUDGER_COUNT = 5

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Quidditch"

MOVEMENT_SPEED = 6
SPRITE_SPEED = 3.5
BULLET_SPEED = 7
SNAPE_BULLET_SPEED = 3
SNAPE_SPEED = 2


class Coin(arcade.Sprite):
    """
    This class represents the coins on our screen. It is a child class of
    the arcade library's "Sprite" class.
    """

    def avoid_sprite(self, player_sprite):
        """
        This function will move the current sprite towards whatever
        other sprite is specified as a parameter.

        We use the 'min' function here to get the sprite to line up with
        the target sprite, and not jump around if the sprite is not off
        an exact multiple of SPRITE_SPEED.
        """

        if player_sprite.center_y <= 50:
            self.center_y = SCREEN_HEIGHT / 2
            self.center_x = SCREEN_WIDTH / 2
        elif player_sprite.center_y >= SCREEN_HEIGHT - 50:
            self.center_y = SCREEN_HEIGHT / 2
            self.center_x = SCREEN_WIDTH / 2
        elif self.center_y < player_sprite.center_y and self.center_y > 0:
            self.center_y -= min(SPRITE_SPEED, player_sprite.center_y - self.center_y)
        elif self.center_y > player_sprite.center_y and self.center_y < SCREEN_HEIGHT:
            self.center_y += min(SPRITE_SPEED, self.center_y - player_sprite.center_y)

        if player_sprite.center_x <= 50:
            self.center_y = SCREEN_HEIGHT / 2
            self.center_x = SCREEN_WIDTH / 2
        elif player_sprite.center_x >= SCREEN_WIDTH - 50:
            self.center_y = SCREEN_HEIGHT / 2
            self.center_x = SCREEN_WIDTH / 2
        elif self.center_x < player_sprite.center_x and self.center_x > 0:
            self.center_x -= min(SPRITE_SPEED, player_sprite.center_x - self.center_x)
        elif self.center_x > player_sprite.center_x and self.center_x < SCREEN_WIDTH:
            self.center_x += min(SPRITE_SPEED, self.center_x - player_sprite.center_x)

    def snape_sprite(self, player_sprite):

        if player_sprite.center_y <= self.center_y:
            self.center_y = self.center_y - SNAPE_SPEED
        elif player_sprite.center_y > self.center_y:
            self.center_y = self.center_y + SNAPE_SPEED

        # if player_sprite.center_x <= (SCREEN_HEIGHT/2):
        #    self.center_x -= min(SPRITE_SPEED, player_sprite.center_x - self.center_x)
        # else:
        #    self.center_x += min(SPRITE_SPEED, self.center_x - player_sprite.center_x)


class Player(arcade.Sprite):

    def update(self):
        """ Move the player """
        # Move player.
        # Remove these lines if physics engine is moving player.
        self.center_x += self.change_x
        self.center_y += self.change_y

        # Check for out-of-bounds
        if self.left < 0:
            self.left = 0
        elif self.right > SCREEN_WIDTH - 1:
            self.right = SCREEN_WIDTH - 1

        if self.bottom < 0:
            self.bottom = 0
        elif self.top > SCREEN_HEIGHT - 1:
            self.top = SCREEN_HEIGHT - 1

    def update_player_snape(self):

        self.center_y += self.change_y

        if self.bottom < 0:
            self.bottom = 0
        elif self.top > SCREEN_HEIGHT - 1:
            self.top = SCREEN_HEIGHT - 1


class GameOverView(arcade.View):
    """ View to show when game is over """

    def __init__(self):
        """ This is run once when we switch to this view """
        super().__init__()
        self.texture = arcade.load_texture("Pictures/Quidditch_Lose.jpg")

        # Reset the viewport, necessary if we have a scrolling game and we need
        # to reset the viewport back to the start so we can see what we draw.
        arcade.set_viewport(0, SCREEN_WIDTH - 1, 0, SCREEN_HEIGHT - 1)

    def on_draw(self):
        """ Draw this view """
        arcade.start_render()
        self.texture.draw_sized(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2,
                                SCREEN_WIDTH, SCREEN_HEIGHT)


class GameOverViewWin(arcade.View):

    def __init__(self):
        """ This is run once when we switch to this view """
        super().__init__()
        self.texture = arcade.load_texture("Pictures/Quidditch_Win.jpg")

        # Reset the viewport, necessary if we have a scrolling game and we need
        # to reset the viewport back to the start so we can see what we draw.
        arcade.set_viewport(0, SCREEN_WIDTH - 1, 0, SCREEN_HEIGHT - 1)

    def on_draw(self):
        """ Draw this view """
        arcade.start_render()
        self.texture.draw_sized(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2,
                                SCREEN_WIDTH, SCREEN_HEIGHT)


class Quidditch(arcade.View):
    """ Our custom Window Class"""

    def __init__(self):
        """ Initializer """
        # Call the parent class initializer
        super().__init__()

        # Set the working directory (where we expect to find files) to the same
        # directory this .py file is in. You can leave this out of your own
        # code, but it is needed to easily run the examples using "python -m"
        # as mentioned at the top of this program.
        file_path = os.path.dirname(os.path.abspath(__file__))
        os.chdir(file_path)

        # Variables that will hold sprite lists
        self.player_list = None
        self.coin_list = None

        # Set up the player info
        self.player_sprite = None
        self.score = 0

        # Track the current state of what key is pressed
        self.left_pressed = False
        self.right_pressed = False
        self.up_pressed = False
        self.down_pressed = False

        # Don't show the mouse cursor
        self.window.set_mouse_visible(False)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        """ Set up the game and initialize the variables. """

        # Sprite lists
        self.player_list = arcade.SpriteList()
        self.coin_list = arcade.SpriteList()

        self.total_time = 30.0

        # Score
        self.score = 0

        # Set up the player
        # Character image from kenney.nl
        self.player_sprite = arcade.Sprite("Pictures/Quidditch_character_models_flying.png",
                                           SPRITE_SCALING_PLAYER)
        self.player_sprite.center_x = 50
        self.player_sprite.center_y = 50
        self.player_list.append(self.player_sprite)

        # Create the coins
        for i in range(COIN_COUNT):
            # Create the coin instance
            # Coin image from kenney.nl
            coin = Coin("Pictures/golden_snitch.png", SPRITE_SCALING_COIN)

            # Position the coin
            coin.center_x = random.randrange(SCREEN_WIDTH)
            coin.center_y = random.randrange(SCREEN_HEIGHT)

            # Add the coin to the lists
            self.coin_list.append(coin)

        self.background = arcade.load_texture("Pictures/Quidditch_fieldv2.png")

        player_info = pd.read_csv('player_info.csv')

    def on_draw(self):
        """ Draw everything """
        arcade.start_render()
        arcade.draw_texture_rectangle(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2, SCREEN_WIDTH, SCREEN_HEIGHT,
                                      self.background)

        seconds = int(self.total_time)

        self.coin_list.draw()
        self.player_list.draw()

        # Put the text on the screen.
        output_time = f"Time: {seconds:02d}"
        output_score = f"Score: {self.score}"

        arcade.draw_text(output_score, 10, 20, arcade.color.BLACK, 14)
        arcade.draw_text(output_time, 10, 50, arcade.color.BLACK, 14)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP:
            self.up_pressed = True
        elif key == arcade.key.DOWN:
            self.down_pressed = True
        elif key == arcade.key.LEFT:
            self.left_pressed = True
        elif key == arcade.key.RIGHT:
            self.right_pressed = True

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP:
            self.up_pressed = False
        elif key == arcade.key.DOWN:
            self.down_pressed = False
        elif key == arcade.key.LEFT:
            self.left_pressed = False
        elif key == arcade.key.RIGHT:
            self.right_pressed = False

    def on_update(self, delta_time):
        """ Movement and game logic """

        self.total_time -= delta_time

        for coin in self.coin_list:
            coin.avoid_sprite(self.player_sprite)

        self.player_sprite.change_x = 0
        self.player_sprite.change_y = 0

        if self.up_pressed and not self.down_pressed:
            self.player_sprite.change_y = MOVEMENT_SPEED
        elif self.down_pressed and not self.up_pressed:
            self.player_sprite.change_y = -MOVEMENT_SPEED
        if self.left_pressed and not self.right_pressed:
            self.player_sprite.change_x = -MOVEMENT_SPEED
        elif self.right_pressed and not self.left_pressed:
            self.player_sprite.change_x = MOVEMENT_SPEED
        self.player_list.update()

        # Generate a list of all sprites that collided with the player.
        hit_list = arcade.check_for_collision_with_list(self.player_sprite, self.coin_list)

        # Loop through each colliding sprite, remove it, and add to the score.
        for coin in hit_list:
            coin.remove_from_sprite_lists()
            self.score += 1

        if self.score == 1:
            view = GameOverViewWin()
            self.window.show_view(view)
            player_info.Win = 1
            player_info.to_csv('player_info.csv')

        if self.total_time <= 0:
            view = GameOverView()
            self.window.show_view(view)
            player_info.Win = 0
            player_info.to_csv('player_info.csv')


class Snape_battle(arcade.View):
    """ Our custom Window Class"""

    def __init__(self):
        """ Initializer """
        # Call the parent class initializer
        super().__init__()

        # Set the working directory (where we expect to find files) to the same
        # directory this .py file is in. You can leave this out of your own
        # code, but it is needed to easily run the examples using "python -m"
        # as mentioned at the top of this program.

        file_path = os.path.dirname(os.path.abspath(__file__))
        os.chdir(file_path)

        self.frame_count = 0

        # Variables that will hold sprite lists
        self.player_list = None
        self.coin_list = None
        self.bullet_list = None
        self.wall_list = None
        self.enemy_bullet_list = None

        # Set up the player info
        self.player_sprite = None
        self.score = 0
        self.snape_health = 100
        self.player_health = player_info.Health[0]

        # Track the current state of what key is pressed
        self.left_pressed = False
        self.right_pressed = False
        self.up_pressed = False
        self.down_pressed = False

        # Don't show the mouse cursor
        self.window.set_mouse_visible(True)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        """ Set up the game and initialize the variables. """

        # Sprite lists
        self.player_list = arcade.SpriteList()
        self.coin_list = arcade.SpriteList()
        self.bullet_list = arcade.SpriteList()
        self.enemy_bullet_list = arcade.SpriteList()
        self.wall_list = arcade.SpriteList()

        self.total_time = 30.0

        # Set up walls

        for y in range(128, SCREEN_HEIGHT, 196):
            wall = arcade.Sprite(":resources:images/tiles/boxCrate_single.png", SPRITE_SCALING_BOX)
            wall.center_x = SCREEN_WIDTH - 150
            wall.center_y = y
            # wall.angle = 45
            self.wall_list.append(wall)

        # Set up the player
        # Character image from kenney.nl
        self.player_sprite = arcade.Sprite("Pictures/Quidditch_character_models_flying.png",
                                           SPRITE_SCALING_PLAYER)
        self.player_sprite.center_x = 30
        self.player_sprite.center_y = SCREEN_HEIGHT / 2
        self.player_list.append(self.player_sprite)

        for i in range(COIN_COUNT):
            coin = Coin("Pictures/SNAPE.jpg", SPRITE_SCALING_SNAPE)

        # Position the coin
            coin.center_x = SCREEN_WIDTH - 50
            coin.center_y = SCREEN_HEIGHT / 2

        # Add the coin to the lists
        self.coin_list.append(coin)

        self.background = arcade.load_texture("Pictures/SNAPE_OFFICE.jpg")

        player_info = pd.read_csv('player_info.csv')

    def on_draw(self):
        """ Draw everything """
        arcade.start_render()
        arcade.draw_texture_rectangle(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2, SCREEN_WIDTH, SCREEN_HEIGHT,
                                      self.background)

        seconds = int(self.total_time)

        self.coin_list.draw()
        self.player_list.draw()
        self.bullet_list.draw()
        self.enemy_bullet_list.draw()
        self.wall_list.draw()

        # Put the text on the screen.
        output_time = f"Time: {seconds:02d}"
        output_snape_health = f"health: {self.snape_health}"
        output_player_health = f"health: {self.player_health}"

        arcade.draw_text(output_player_health, 10, 20, arcade.color.BLACK, 14)
        arcade.draw_text(output_time, 10, 50, arcade.color.BLACK, 14)
        arcade.draw_text(output_snape_health,SCREEN_WIDTH - 120, 20, arcade.color.RED, 14)

    def on_mouse_press(self, x, y, button, modifiers):
        """ Called whenever the mouse button is clicked. """

        # Create a bullet
        bullet = arcade.Sprite(":resources:images/space_shooter/laserBlue01.png", SPRITE_SCALING_LASER)

        # Position the bullet at the player's current location
        start_x = self.player_sprite.center_x
        start_y = self.player_sprite.center_y
        bullet.center_x = start_x
        bullet.center_y = start_y

        # Get from the mouse the destination location for the bullet
        # IMPORTANT! If you have a scrolling screen, you will also need
        # to add in self.view_bottom and self.view_left.
        dest_x = x
        dest_y = y

        # Do math to calculate how to get the bullet to the destination.
        # Calculation the angle in radians between the start points
        # and end points. This is the angle the bullet will travel.
        x_diff = dest_x - start_x
        y_diff = dest_y - start_y
        angle = math.atan2(y_diff, x_diff)

        # Angle the bullet sprite so it doesn't look like it is flying
        # sideways.
        bullet.angle = math.degrees(angle)
        #print(f"Bullet angle: {bullet.angle:.2f}")

        # Taking into account the angle, calculate our change_x
        # and change_y. Velocity is how fast the bullet travels.
        bullet.change_x = math.cos(angle) * BULLET_SPEED
        bullet.change_y = math.sin(angle) * BULLET_SPEED

        # Add the bullet to the appropriate lists
        self.bullet_list.append(bullet)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP:
            self.up_pressed = True
        elif key == arcade.key.DOWN:
            self.down_pressed = True
        elif key == arcade.key.LEFT:
            self.left_pressed = True
        elif key == arcade.key.RIGHT:
            self.right_pressed = True

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP:
            self.up_pressed = False
        elif key == arcade.key.DOWN:
            self.down_pressed = False
        elif key == arcade.key.LEFT:
            self.left_pressed = False
        elif key == arcade.key.RIGHT:
            self.right_pressed = False

    def on_update(self, delta_time):
        """ Movement and game logic """

        self.total_time -= delta_time
        self.frame_count += 1

        for coin in self.coin_list:
            coin.snape_sprite(self.player_sprite)

        self.player_sprite.change_x = 0
        self.player_sprite.change_y = 0

        if self.up_pressed and not self.down_pressed:
            self.player_sprite.change_y = MOVEMENT_SPEED
        elif self.down_pressed and not self.up_pressed:
            self.player_sprite.change_y = -MOVEMENT_SPEED
        self.player_list.update()
        self.enemy_bullet_list.update()
        self.bullet_list.update()
        self.enemy_bullet_list.update()

        for enemy in self.coin_list:
            e_x_start = enemy.center_x
            e_y_start = enemy.center_y
            e_dest_x = self.player_sprite.center_x
            e_dest_y = self.player_sprite.center_y
            e_x_diff = e_dest_x - e_x_start
            e_y_diff = e_dest_y - e_y_start
            e_angle = math.atan2(e_y_diff, e_x_diff)

            enemy.angle = math.degrees(e_angle) - 180

            if self.frame_count % 60 == 0:
                e_bullet = arcade.Sprite(":resources:images/space_shooter/laserRed01.png")
                e_bullet.center_x = e_x_start
                e_bullet.center_y = e_y_start
                e_bullet.angle = math.degrees(e_angle) - 90

                e_bullet.change_x = math.cos(e_angle) * SNAPE_BULLET_SPEED
                e_bullet.change_y = math.sin(e_angle) * SNAPE_BULLET_SPEED

                self.enemy_bullet_list.append(e_bullet)

        for e_bullet in self.enemy_bullet_list:
            player_hit_list = arcade.check_for_collision_with_list(e_bullet, self.player_list)

            if len(player_hit_list) > 0:
                e_bullet.remove_from_sprite_lists()

            for x in player_hit_list:
                self.player_health -= 10
        # Generate a list of all sprites that collided with the player.
        #hit_list = arcade.check_for_collision_with_list(bullet, self.coin_list)


        # Loop through each colliding sprite, remove it, and add to the score.
        for bullet in self.bullet_list:
            hit_list = arcade.check_for_collision_with_list(bullet, self.coin_list)
            hit_walls_list = arcade.check_for_collision_with_list(bullet, self.wall_list)

            if len(hit_list) > 0:
                bullet.remove_from_sprite_lists()

            if len(hit_walls_list) > 0:
                bullet.remove_from_sprite_lists()

            #if bullet.bottom > self.width or bullet.top < 0 or bullet.right < 0 or bullet.left > self.width:
            #    bullet.remove_from_sprite_lists()

            for coin in hit_list:
                #coin.remove_from_sprite_lists()
                self.snape_health -= player_info['Spell Power'][0]

        if self.snape_health <= 0:
            view = GameOverViewWin()
            self.window.show_view(view)
            player_info.Win = 1
            player_info.to_csv('player_info.csv')

        if self.total_time <= 0 or self.player_health <= 0:
            view = GameOverView()
            self.window.show_view(view)
            player_info.Win = 0
            player_info.to_csv('player_info.csv')


def main():
    """ Main function """
    window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    start_view = Quidditch()
    window.show_view(start_view)
    start_view.setup()
    arcade.run()
    player_info = pd.read_csv('player_info.csv')


def main_snape():
    window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    start_view = Snape_battle()
    window.show_view(start_view)
    start_view.setup()
    arcade.run()
    player_info = pd.read_csv('player_info.csv')


def status():
    print('\nWizard Status\n')
    print(player_info)


def player_input(question, options):
    while True:
        global sel
        sel = input(question)
        if sel not in options:
            print('\nInvalid Selection, please select a valid choice')
            continue
        else:
            return sel


def house_bonus():
    if player_info.House[0] == 'Gryffindor':
        player_info['Luck'] = player_info['Luck'] + 15
    elif player_info.House[0] == 'Ravenclaw':
        player_info['Spell Power'] = player_info['Spell Power'] + 10
    elif player_info.House[0] == 'Slytherin':
        player_info['Galleon'] = player_info['Galleon'] + 10
    else:
        player_info['Health'] = player_info['Health'] + 15

def GUI():

    root = tk.Tk()

    HEIGHT = 600
    WIDTH = 900

    canvas = Canvas(root, height = HEIGHT, width = WIDTH)
    canvas.pack()

    background_image = Image.open("hogwarts-adventure-game/Pictures/sortinghat.png")
    resized = background_image.resize((900,600), Image.ANTIALIAS)
    bkgrd = ImageTk.PhotoImage(resized)
    background_label = Label(root, image=bkgrd)
    background_label.place(relheight=1, relwidth=1)


    label = Label(root, bg = 'grey', justify= CENTER, text='Pick your House!', font= 'Papyrus', relief=RIDGE)
    label.place(relheight=0.09, relwidth=0.3, relx=0.35,rely=0.18)

    lbox = Listbox(root, bg = 'grey', bd = 8, justify = CENTER, selectmode=BROWSE, font = 'Papyrus', relief=SUNKEN)
    lbox.insert(1, 'Gryffindor')
    lbox.insert(2, 'Slytherin') 
    lbox.insert(3, 'Hufflepuff') 
    lbox.insert(4, 'Ravenclaw')
    lbox.place(relheight=0.24, relwidth=0.3, relx=0.35,rely=0.3)

    def message():
        messagebox.showinfo("House", "Congratulations! You have chosen the " + str(lbox.get(ACTIVE)) + " house!")
        exit()

    submit_button = Button(root, bg = 'grey', text = 'Submit', font = 'Papyrus', relief=RAISED, command = message)
    submit_button.place(relheight= 0.1, relwidth= 0.15, relx=0.43, rely=0.57)

    root.mainloop()

# def GUI(label_txt, **options):
#     #label_txt, **options are the parameters.
#     """
#     This function will create a selection screen with text box selections that a player can
#     select from.

#     :param label_txt: (string) Label for GUI text box
#     :param **options: (dict) include number first then option name
#     :return:
#     """
#     root = tk.Tk()

#     canvas = Canvas(root, height=SCREEN_HEIGHT, width=SCREEN_WIDTH)
#     canvas.pack()

#     # image_path = 'Pictures/' + pic + '.png'
#     background_image = Image.open('hogwarts-adventure-game/Pictures/sortinghat.png')
#     resized = background_image.resize((SCREEN_WIDTH, SCREEN_HEIGHT), Image.ANTIALIAS)
#     bkgrd = ImageTk.PhotoImage(resized)
#     background_label = Label(root, image=bkgrd)
#     background_label.place(relheight=1, relwidth=1)

#     label = Label(root, bg='grey', justify=CENTER, text=label_txt, font='Papyrus', relief=RIDGE)
#     label.place(relheight=0.20, relwidth=0.5, relx=0.5, rely=0.15, anchor=CENTER)

#     lbox = Listbox(root, bg='grey', bd=8, justify=CENTER, selectmode=BROWSE, font='Papyrus', relief=SUNKEN)

#     for y in options:
#         y_int = int(y)
#         lbox.insert(y_int, options[y])

#     lbox.place(relheight=0.24, relwidth=0.3, relx=0.35, rely=0.3)

#     def message():
#         messagebox.showinfo("House", "Congratulations! You have chosen the " + str(lbox.get(ACTIVE)) + " house!")
#         # selection = lbox.curselection()
#         root.quit()

#     # selection = lbox.curselection()
#     # selection = lbox.get(ACTIVE)
#     submit_button = Button(root, bg='grey', text='Submit', font='Papyrus', relief=RAISED, command=message)
#     submit_button.place(relheight=0.1, relwidth=0.15, relx=0.43, rely=0.57)
#     selection = lbox.get(ACTIVE)
#     root.quit()

#     root.mainloop()

#     return selection


def player_bonus(amount, attributes):
    """

    This function adds specified amount to player attributes. It also check to see if the player has too little
    attributes. If attributes go below 0, the difference is removed from health. Also check if player health is
    at 0 or below.

    :param amount: (double) The amount to be added
    :param attributes: (string) Attributes to adjust. Accepts list
    :return:
    """
    for i in attributes:
        player_info[i] = player_info[i] + amount

    for x in ['Luck', 'Galleon', 'Spell Power']:
        if player_info[x][0] < 0:
            amt = player_info[x][0]
            print('\nYOU WENT NEGATIVE! YOU WILL PAY FOR THIS\n' + name +
                  ' lost ' + str(amt) + ' health due to poor math skills')
            player_info['Health'] = player_info['Health'] + player_info[x]
            player_info[x] = 1
            input('\nHit enter to move on:')

    if player_info.Health[0] <= 0:
        print('\nYou have lost all health and have perished... Be more careful next time')
        exit()


player_info = pd.DataFrame(columns=['Name', 'Health', 'Luck', 'Galleon', 'Spell Power', 'House', 'Win'])
player_info.loc[player_info.shape[0]] = [None, 100, 50, 50, 10, None, 0]
name = input("Player Name: ")
player_info.Name = name

player_info.to_csv('player_info.csv')

print('\n WELCOME TO HOGWARTS! \n\n')
print('Welcome ' + name + ' to the wild world of wizardry. You have been accepted to the top \n wizard '
                          'school in the nation; Hogwarts. You couldnt come at a better time, '
                          '\n he who shall not be named has escaped and is terrorizing the school '
                          'students \n and staff. It is up to you to defeat him and save the school. \n\n '
                          'You will make strategic decisions that will decide your fate. \n Gather items, '
                          'spells and Galleon that will help move through the castle. \n You start with 100 '
                          'health, if you lose all your health you perish and Lord V \n takes over '
                          'Hogwarts.')
input('\n\n Press Enter to start your adventure: ')

print('\nSorting Hat: Hello ' + name + '\nWelcome to Hogwarts. Before we get you settled in, you need a house to '
                                       '\nbelong to. Choose wisely as this will have an impact on your luck, spells, '
                                       '\nhealth and Galleon down the road '
                                       '\nWith that in mind, what would you like to do?')

status()

print('\n' + tabulate(
    [['Gryffindor', 'Luck'], ['Ravenclaw', 'Spells'], ['Slytherin', 'Galleon'], ['Hufflepuff', 'Health']],
    headers=['House', 'Attribute Bonus'], tablefmt='orgtbl'))

houses = ['Gryffindor', 'Ravenclaw', 'Slytherin', 'Hufflepuff']

# selection = GUI('\n1 = Sorting hat selection at 0 Galleon \n2 = Bribe sorting hat at 10 Galleon',
# **{'1': '1', '2': '2'})

player_input('\n1 = Sorting hat selection at 0 Galleon \n2 = Bribe sorting hat at 10 Galleon \nSelection:', ['1', '2'])

if sel == '1':
    house_sel = random.randint(1, 4)
    player_info.House = houses[(house_sel - 1)]
elif sel == '2':
    GUI()
    #maybe remove the message box stating what house was chosen. 
    #Instead, have the program exit after clicking the submit button and return to the code script. input information in the player_info
    #and continue with house_desc.
    
    player_info.Galleon = player_info.Galleon - 10
    # print('\nSorting Hat: This wizard has incredible spirit! *counts Galleon* Which house would you like?\n')
    # player_input('1 = Gryffindor \n2 = Ravenclaw \n3 = Slytherin \n4 = Hufflepuff \nSelection:', ['1', '2', '3', '4'])
    # sel = int(sel)
    # player_info.House = houses[(sel - 1)]
    # print('\nSorting Hat: This wizard has incredible spirit! *counts Galleon* Which house would you like?\n')
    # player_input('1 = Gryffindor \n2 = Ravenclaw \n3 = Slytherin \n4 = Hufflepuff \nSelection:', ['1','2', '3', '4'])
    # sel = int(sel)
    # player_info.House = houses[(sel - 1)]

house_desc = {'Gryffindor': '\nAs a member of Gryffindor, you are courageous and brave. Your traits have earned you '
                            '\nthe respect of your peers. This respect may be translated in the form of luck on the '
                            '\nbattlefield. This luck may be the diffence between life and death.'
                            '\nthe enemy. +15 Luck in battles',
              'Ravenclaw': '\nAs a member of Ravenclaw, you are intelligent and wise. This intelligence has pushed '
                           '\nyou to learn the most powerful spells. Use this new found power as an advantage in your '
                           '\nfuture battles. +10 Attack on base moves',
              'Slytherin': '\nAs a member of Slytherin, you are cunning and resourceful. You have used your '
                           '\nresourcefulness to aquire excess Galleon. Use this Galleon to gain aquire the best items '
                           '\nand pay your way to victory. +10 Galleon each battle',
              'Hufflepuff': '\nAs a member of Hufflepuff, you are hardworking and patient. This hardwork has helped '
                            '\nyou become very physically fit. Use this fitness and patience to outlast your '
                            '\nadversaries. +15 Health each battle'}
player_house = player_info.House[0]

print('\nCongratulations, you have been placed into the ' + player_house + ' house!\n' + house_desc[player_house])
input('\nHit Enter to move on')

print('\nYou begin unloading you things when you are bombarded by a bunch of your housemates.\n '
      '\nHousemate: Hey ' + name + '! We were about to compete in Quidditch but we are short a player. Its important '
                                   '\nthat ' + player_house + 'is represented! We dont want to give away points. '
                                                              '\nWhat will you do?')
house_bonus()
status()
player_input('\n1 = Join the game \n2 = Continue getting settled \nSelection:', ['1', '2'])

if sel == '1':
    print(
        '\nAwesome! Lets get going. \n\nAs the match carries on the opponents start to take over. You decide that '
        '\nthere is only one way to win this, THE GOLDEN SNITCH. There is only 15 seconds left on the clock. Can you '
        '\ncatch it?')
    input('\n\nHit enter to begin game:')
    main()

else:
    print('\nCome on man, you are going to leave us hanging already? We arent going to let your unpacking ruin our '
          '\nchances! *Housemates drag you onto the Quidditch field*\n -10 Health\n\nAs the match carries on the '
          'opponents start to take over. You decide that '
          '\nthere is only one way to win this, THE GOLDEN SNITCH. There is only 30 seconds left on the clock. Can you '
          '\ncatch it?')
    player_info.Health = player_info.Health - 10
    input('\n\nHit enter to begin game:')
    main()

if player_info.Win[0] == 1:
    print('\n\nNICE GRAB!!! Youre the best ' + name + '!\n +5 Luck\n +5 Galleon')
    player_bonus(5, ['Luck', 'Galleon'])
    input('\nPress enter to move on:')
else:
    print('\n\nDARN!!! We lost again... at this rate we will never be the top house.\n -5 Health\n -5 Luck')
    player_bonus(-5, ['Health', 'Luck'])
    input('\nPress enter to move on:')

print('\n\nYou return to the ' + player_house + ' dormitory and finish getting your things unpacked.\n\nNow settled '
                                                'in, where would you like to go?')

actions = 0
while actions < 2:
    player_input('\n1 = The Great Hall\n2 = The Astronomy Room\n3 = The Library\n4 = Professor Snapes '
                 'Office\nSelection:',
                 ['1', '2', '3', '4'])
    if actions > 0:
        while sel == sel_1:
            print('\nYou have already travelled here, there is nothing more to see. Make another selection')
            player_input('\n1 = The Great Hall\n2 = The Astronomy Room\n3 = The Library\n4 = Professor Snapes '
                         'Office\nSelection:',
                         ['1', '2', '3', '4'])
    sel_1 = sel

    if sel == '1':
        # Great Hall Option
        print('\nYou walk into the Great Hall and see the grandest meal of your life!'
              '\nThere appears to be an endless amount of food.'
              '\nEat as much food as you can and earn points for ' + player_info.House + '.')
        input('\n\nHit enter to begin feasting:')
        # Place holder for food game
        actions = actions + 1
        continue
    elif sel == '2':
        # Astronomy Room Option
        actions = actions + 1
        continue
    elif sel == '3':
        # Library Option
        actions = actions + 1
        continue
    else:
        # Professor Snapes Office Option
        if player_house == 'Slytherin':
            print('\nWhat the heck are you doing ' + name + ', get out of my office!')
            print('\n*Notices Slytherin patch*\n\nWait one moment, you must have great intelligence and savvy '
                  'to be in the Slytherin house. What would you like from me?')
            slytherin_1 = player_input('\n1 = Ask for potions lesson\n2 = Ask for Defense against dark arts lesson\n3 '
                                       '= Challenge to a duel to receive secret information '
                                       '\nSelection:', ['1', '2', '3'])
            if slytherin_1 == '1':
                print('\nYou came to the right staff member. I will do you one better and offer you one of my '
                      '\nnewest potions. This potion will increase your spell power by +5 points but will cost '
                      '\nyou 15 Galleon. Will you take this deal? ')
                status()
                slytherin_2 = player_input('\n1 = Buy potion\n2 = Pass on Deal\nSelection:', ['1', '2'])
                if slytherin_2 == '1':
                    print('\n\nGood choice...')
                    player_bonus(5, ['Spell Power'])
                    player_bonus(-15, ['Galleon'])
                    actions = actions + 1
                else:
                    print('\n\nYou are missing out...')
                    actions = actions + 1
            elif slytherin_1 == '2':
                print('\nYou came to the right staff member. I will do you one better and offer you one of my '
                      '\nnewest defensive tools. This tool will give you a one time health increase by 30 but will '
                      'cost '
                      '\nyou 30 Galleon. Will you take this deal? ')
                status()
                slytherin_3 = player_input('\n1 = Buy tool\n2 = Pass on Deal\nSelection:', ['1', '2'])
                if slytherin_3 == '1':
                    print('\n\nGood choice...')
                    player_bonus(30, ['Health'])
                    player_bonus(-30, ['Galleon'])
                    actions = actions + 1
                else:
                    print('\n\nYou are missing out...')
                    actions = actions + 1
            else:
                print(
                    '\n\nI thought as a Slytherin you would have better common sense. It appears I need to teach you '
                    'a new lesson. ')
                input('\n\nHit enter to begin battle with Prof Snape:')
                house_bonus()
                main_snape()
                actions = actions + 1
        else:
            print('\n\nWhat the heck are you doing ' + name + ', you shouldnt be here! Looks like you should be '
                                                              'taught a lesson...')
            input('\n\nHit enter to begin battle with Prof Snape:')
            house_bonus()
            main_snape()
            actions = actions + 1
        continue
