
#need to create a template for the GUI. Depending on the interface, choose different
#picure background, different number of possible solutions/options,
#different prompt/question in Label
#create functions that will allow this variability.

import tkinter as tk
from tkinter import Button, Canvas, Label, Listbox, messagebox
from tkinter.constants import ACTIVE, BROWSE, CENTER, RAISED, RIDGE, SUNKEN
from PIL import ImageTk, Image

def GUI():

    root = tk.Tk()

    HEIGHT = 600
    WIDTH = 900

    canvas = Canvas(root, height = HEIGHT, width = WIDTH)
    canvas.pack()

    background_image = Image.open("hogwarts-adventure-game/Pictures/sortinghat.png")
    resized = background_image.resize((900,600), Image.ANTIALIAS)
    bkgrd = ImageTk.PhotoImage(resized)
    background_label = Label(root, image=bkgrd)
    background_label.place(relheight=1, relwidth=1)


    label = Label(root, bg = 'grey', justify= CENTER, text='Pick your House!', font= 'Papyrus', relief=RIDGE)
    label.place(relheight=0.09, relwidth=0.3, relx=0.35,rely=0.18)

    lbox = Listbox(root, bg = 'grey', bd = 8, justify = CENTER, selectmode=BROWSE, font = 'Papyrus', relief=SUNKEN)
    lbox.insert(1, 'Gryffindor')
    lbox.insert(2, 'Slytherin') 
    lbox.insert(3, 'Hufflepuff') 
    lbox.insert(4, 'Ravenclaw')
    lbox.place(relheight=0.24, relwidth=0.3, relx=0.35,rely=0.3)

    def message():
        messagebox.showinfo("House", "Congratulations! You have chosen the " + str(lbox.get(ACTIVE)) + " house!")
        exit()

    submit_button = Button(root, bg = 'grey', text = 'Submit', font = 'Papyrus', relief=RAISED, command = message)
    submit_button.place(relheight= 0.1, relwidth= 0.15, relx=0.43, rely=0.57)

    root.mainloop()

GUI()