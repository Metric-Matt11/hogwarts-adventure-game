from os import close, truncate
import tkinter

# THINGS TO ADD TO PROJECT
# extend the story and implement either ways to get more lives (if you have a streak of '' of choosing right, get 1 extra life, etc. something)
# create battles where it takes into account your number of lives and this correlates w a fair-die or a weighted die leaning to better or worse probabilit


'''#Loop to play again
def replay():
    input("Do you want to play again? \n").lower()
    if replay == "yes":
        play_game()
    else:
        exit()'''

# Player Info
player_name = input("Player Name: ")
player_age = input('Age: ')
player_age = int(player_age)

if player_age < 18:
    print("too young to play :(")
    exit()

game_difficulty = input('Choose Difficulty: Easy | Normal | Hard \n').lower()
'''game difficulty will change settings in categories such as number of choices, probability of surviving as a result'''

# Introduction
print('\n FIND THE KEY SAFELY! \n\n')
print('Okay ' + player_name + ', you are being chased by giant thumbs and the only way you can out run them is with '
                              'your car. \n Unfortunately, you lost your car keys somewhere. \n Find them before its '
                              'too late.')
if game_difficulty == 'easy':
    print('\n Choose the right path, and move on. \n Choose the wrong path, and lose a life. \n After 3 lives, you lose.')
if game_difficulty == 'normal':
    print('\n Choose the right path, and move on. \n Choose the wrong path, and lose a life. \n After 2 lives, you lose.')
if game_difficulty == 'hard':
    print('\n Choose the right path, and move on. \n Choose the wrong path, and lose a life. \n After 1 life, you lose.')

question = input('\n Are you ready? ').lower()
if question != 'yes':
    print('Better luck next time')
    exit()
else:
    pass

# Appending to player info file
with open('playerinfo.txt', 'a') as playerfile:
    playerfile.write(player_name + ',' + str(player_age) +
                     ',' + game_difficulty + ',')

## QUOTED IS CORRECT PATH ##
## IF YOU CHOOSE THE WRONG PATH, YOU ARE STILL BROUGHT TO THE CORRECT ONE, JUST LOSE A LIFE ##

# Level 1 (HOUSE 1, "HOUSE 2", HOUSE 3)

lives_lost = 0


def game_over1():
    if lives_lost == 3:
        print("You're out of lives! Better Luck Next Time!")
    with open('playerinfo.txt', 'a') as playerfile:
        playerfile.write('Fail \n')
        # replay()
        exit()


def game_over2():
    if lives_lost == 2:
        print("You're out of lives! Better Luck Next Time!")
    with open('playerinfo.txt', 'a') as playerfile:
        playerfile.write('Fail \n')
        # replay()
        exit()


def game_over3():
    if lives_lost == 1:
        print("You're out of lives! Better Luck Next Time!")
    with open('playerinfo.txt', 'a') as playerfile:
        playerfile.write('Fail \n')
        # replay()
        exit()


if game_difficulty == 'easy':
    while lives_lost <= 2:
        print("\n\n You've been running for hours and you're disoriented. \n You're on your street but you forget what house is yours.\n".lower())
        level_1 = input('House 1 | House 2 | House 3 \n ')
        if level_1 == 'House 2':
            print('CORRECT')
        elif level_1 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1

        # Level 2 (MAINFLOOR, UPSTAIRS, "DOWNSTAIRS")

        print("\n\n You're in House 2. \n What floor could the key be on?".lower())
        level_2 = input('Upstairs | Main Floor | Downstairs \n ')
        if level_2 == 'Downstairs':
            print('CORRECT')
        elif level_2 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1

        # Level 3 ("BEDROOM", BATHROOM, OFFICE)

        print("\n\n You're downstairs. \n What room could the key be hidden?".lower())
        level_3 = input('Bedroom | Bathroom | Office \n ')
        if level_3 == 'Bedroom':
            print('CORRECT')
        elif level_3 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over1()

        # Level 4 ("CLOSET", UNDER BED, DESK)

        print("\n\n You're in the bedroom. \n Where could it be?".lower())
        level_4 = input('Closet | Under Bed | Desk \n ')
        if level_4 == 'Closet':
            print('CORRECT')
        elif level_4 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over1()

        # Level 5 (IN THE CLOTHES, BEHIND THE DRYWALL, "IN THE LIGHT CAP")

        print("\n\n You're in the closet (with pete). \n Where could you have left your keys?".lower())
        level_5 = input(
            'In your clothes | Behind the Drywall | In the light cap \n ')
        if level_5 == 'In the light cap':
            print('CORRECT')
            print(
                'CONGRATS! YOU FOUND THE KEY! \n You got to your car just in time to get away!')
            with open('playerinfo.txt', 'a') as playerfile:
                playerfile.write('Pass \n')
                # replay()
                exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over1()

if game_difficulty == 'normal':
    while lives_lost <= 1:
        print("\n\n You've been running for hours and you're disoriented. \n You're on your street but you forget what house is yours.".lower())
        level_1 = input('House 1 | House 2 | House 3 \n ')
        if level_1 == 'house 2':
            print('CORRECT')
        elif level_1 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1

        # Level 2 (MAINFLOOR, UPSTAIRS, "DOWNSTAIRS")

        print("\n\n You're in House 2. \n What floor could the key be on?".lower())
        level_2 = input('Upstairs | Main Floor | Downstairs \n ')
        if level_2 == 'downstairs':
            print('CORRECT')
        elif level_2 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over2()

        # Level 3 ("BEDROOM", BATHROOM, OFFICE)

        print("\n\n You're downstairs. \n What room could the key be hidden?".lower())
        level_3 = input('Bedroom | Bathroom | Office \n ')
        if level_3 == 'bedroom':
            print('CORRECT')
        elif level_3 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over2()

        # Level 4 ("CLOSET", UNDER BED, DESK)

        print("\n\n You're in the bedroom. \n Where could it be?".lower())
        level_4 = input('Closet | Under Bed | Desk \n ')
        if level_4 == 'closet':
            print('CORRECT')
        elif level_4 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over2()

        # Level 5 (IN THE CLOTHES, BEHIND THE DRYWALL, "IN THE LIGHT CAP")

        print("\n\n You're in the closet (with pete). \n Where could you have left your keys?".lower())
        level_5 = input(
            'In your clothes | Behind the Drywall | In the light cap \n ')
        if level_5 == 'in the light cap':
            print('CORRECT')
            print(
                'CONGRATS! YOU FOUND THE KEY! \n You got to your car just in time to get away!')
            with open('playerinfo.txt', 'a') as playerfile:
                playerfile.write('Pass \n')
                # replay()
                exit()
        elif level_5 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over2()

if game_difficulty == 'hard':
    while lives_lost == 0:
        print("\n\n You've been running for hours and you're disoriented. \n You're on your street but you forget what house is yours.".lower())
        level_1 = input('House 1 | House 2 | House 3 \n ')
        if level_1 == 'house 2':
            print('CORRECT')
        elif level_1 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over3()

        # Level 2 (MAINFLOOR, UPSTAIRS, "DOWNSTAIRS")

        print("\n\n You're in House 2. \n What floor could the key be on?".lower())
        level_2 = input('Upstairs | Main Floor | Downstairs \n ')
        if level_2 == 'downstairs':
            print('CORRECT')
        elif level_2 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over3()

        # Level 3 ("BEDROOM", BATHROOM, OFFICE)

        print("\n\n You're downstairs. \n What room could the key be hidden?".lower())
        level_3 = input('Bedroom | Bathroom | Office \n ')
        if level_3 == 'bedroom':
            print('CORRECT')
        elif level_3 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over3()

        # Level 4 ("CLOSET", UNDER BED, DESK)

        print("\n\n You're in the bedroom. \n Where could it be?".lower())
        level_4 = input('Closet | Under Bed | Desk \n ')
        if level_4 == 'closet':
            print('CORRECT')
        elif level_4 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over3()

        # Level 5 (IN THE CLOTHES, BEHIND THE DRYWALL, "IN THE LIGHT CAP")

        print("\n\n You're in the closet (with pete). \n Where could you have left your keys?".lower())
        level_5 = input(
            'In your clothes | Behind the Drywall | In the light cap \n ')
        if level_5 == 'in the light cap':
            print('CORRECT')
            print(
                'CONGRATS! YOU FOUND THE KEY! \n You got to your car just in time to get away!')
            with open('playerinfo.txt', 'a') as playerfile:
                playerfile.write('Pass \n')
                # replay()
                exit()
        elif level_5 == 'quit':
            exit()
        else:
            print('WRONG')
            lives_lost += 1
            game_over3()

playerfile.write('\n')
playerfile.close()
