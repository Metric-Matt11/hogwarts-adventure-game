"""
Sprite Follow Player

This moves towards the player in both the x and y direction.

Artwork from https://kenney.nl

If Python and Arcade are installed, this example can be run from the command line with:
python -m arcade.examples.sprite_follow_simple
"""

import random
import arcade
import math
import os

# --- Constants ---
SPRITE_SCALING_PLAYER = 0.4
SPRITE_SCALING_COIN = 0.05
SPRITE_SCALING_LASER = 0.8
COIN_COUNT = 1
BlUDGER_COUNT = 5

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Quidditch"

MOVEMENT_SPEED = 6
SPRITE_SPEED = 3.5


class Coin(arcade.Sprite):
    """
    This class represents the coins on our screen. It is a child class of
    the arcade library's "Sprite" class.
    """

    def avoid_sprite(self, player_sprite):
        """
        This function will move the current sprite towards whatever
        other sprite is specified as a parameter.

        We use the 'min' function here to get the sprite to line up with
        the target sprite, and not jump around if the sprite is not off
        an exact multiple of SPRITE_SPEED.
        """

        if player_sprite.center_y <= 50:
            self.center_y = SCREEN_HEIGHT/2
            self.center_x = SCREEN_WIDTH/2
        elif player_sprite.center_y >= SCREEN_HEIGHT - 50:
            self.center_y = SCREEN_HEIGHT/2
            self.center_x = SCREEN_WIDTH/2
        elif self.center_y < player_sprite.center_y and self.center_y > 0:
            self.center_y -= min(SPRITE_SPEED, player_sprite.center_y - self.center_y)
        elif self.center_y > player_sprite.center_y and self.center_y < SCREEN_HEIGHT:
            self.center_y += min(SPRITE_SPEED, self.center_y - player_sprite.center_y)

        if player_sprite.center_x <= 50:
            self.center_y = SCREEN_HEIGHT/2
            self.center_x = SCREEN_WIDTH/2
        elif player_sprite.center_x >= SCREEN_WIDTH - 50:
            self.center_y = SCREEN_HEIGHT/2
            self.center_x = SCREEN_WIDTH/2
        elif self.center_x < player_sprite.center_x and self.center_x > 0:
            self.center_x -= min(SPRITE_SPEED, player_sprite.center_x - self.center_x)
        elif self.center_x > player_sprite.center_x and self.center_x < SCREEN_WIDTH:
            self.center_x += min(SPRITE_SPEED, self.center_x - player_sprite.center_x)


class Player(arcade.Sprite):

    def update(self):
        """ Move the player """
        # Move player.
        # Remove these lines if physics engine is moving player.
        self.center_x += self.change_x
        self.center_y += self.change_y

        # Check for out-of-bounds
        if self.left < 0:
            self.left = 0
        elif self.right > SCREEN_WIDTH - 1:
            self.right = SCREEN_WIDTH - 1

        if self.bottom < 0:
            self.bottom = 0
        elif self.top > SCREEN_HEIGHT - 1:
            self.top = SCREEN_HEIGHT - 1


class MyGame(arcade.Window):
    """ Our custom Window Class"""

    def __init__(self):
        """ Initializer """
        # Call the parent class initializer
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)

        # Set the working directory (where we expect to find files) to the same
        # directory this .py file is in. You can leave this out of your own
        # code, but it is needed to easily run the examples using "python -m"
        # as mentioned at the top of this program.
        file_path = os.path.dirname(os.path.abspath(__file__))
        os.chdir(file_path)

        # Variables that will hold sprite lists
        self.player_list = None
        self.coin_list = None

        # Set up the player info
        self.player_sprite = None
        self.score = 0

        # Track the current state of what key is pressed
        self.left_pressed = False
        self.right_pressed = False
        self.up_pressed = False
        self.down_pressed = False

        # Don't show the mouse cursor
        self.set_mouse_visible(False)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        """ Set up the game and initialize the variables. """
        self.total_time = 15.0

        # Sprite lists
        self.player_list = arcade.SpriteList()
        self.coin_list = arcade.SpriteList()

        # Score
        self.score = 0

        # Set up the player
        # Character image from kenney.nl
        self.player_sprite = arcade.Sprite("Pictures/Quidditch_character_models_flying.png",
                                           SPRITE_SCALING_PLAYER)
        self.player_sprite.center_x = 50
        self.player_sprite.center_y = 50
        self.player_list.append(self.player_sprite)

        # Create the coins
        for i in range(COIN_COUNT):
            # Create the coin instance
            # Coin image from kenney.nl
            coin = Coin("Pictures/golden_snitch.png", SPRITE_SCALING_COIN)

            # Position the coin
            coin.center_x = random.randrange(SCREEN_WIDTH)
            coin.center_y = random.randrange(SCREEN_HEIGHT)

            # Add the coin to the lists
            self.coin_list.append(coin)

        self.background = arcade.load_texture("Pictures/Quidditch_fieldv2.png")

    def on_draw(self):
        """ Draw everything """
        arcade.start_render()
        arcade.draw_texture_rectangle(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2, SCREEN_WIDTH, SCREEN_HEIGHT,
                                      self.background)

        ##minutes = int(self.total_time) // 60
        seconds = int(self.total_time)

        self.coin_list.draw()
        self.player_list.draw()

        # Put the text on the screen.
        output_time = f"Time: {seconds:02d}"
        output_score = f"Score: {self.score}"
        arcade.draw_text(output_score, 10, 20, arcade.color.BLACK, 14)
        arcade.draw_text(output_time, 10, 50, arcade.color.BLACK, 14)
    #def on_mouse_motion(self, x, y, dx, dy):
        #""" Handle Mouse Motion """

        # Move the center of the player sprite to match the mouse x, y
        #self.player_sprite.center_x = x
        #self.player_sprite.center_y = y

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP:
            self.up_pressed = True
        elif key == arcade.key.DOWN:
            self.down_pressed = True
        elif key == arcade.key.LEFT:
            self.left_pressed = True
        elif key == arcade.key.RIGHT:
            self.right_pressed = True

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP:
            self.up_pressed = False
        elif key == arcade.key.DOWN:
            self.down_pressed = False
        elif key == arcade.key.LEFT:
            self.left_pressed = False
        elif key == arcade.key.RIGHT:
            self.right_pressed = False

    def on_update(self, delta_time):
        """ Movement and game logic """

        self.total_time -= delta_time

        for coin in self.coin_list:
            coin.avoid_sprite(self.player_sprite)

        self.player_sprite.change_x = 0
        self.player_sprite.change_y = 0

        if self.up_pressed and not self.down_pressed:
            self.player_sprite.change_y = MOVEMENT_SPEED
        elif self.down_pressed and not self.up_pressed:
            self.player_sprite.change_y = -MOVEMENT_SPEED
        if self.left_pressed and not self.right_pressed:
            self.player_sprite.change_x = -MOVEMENT_SPEED
        elif self.right_pressed and not self.left_pressed:
            self.player_sprite.change_x = MOVEMENT_SPEED
        self.player_list.update()

        # Generate a list of all sprites that collided with the player.
        hit_list = arcade.check_for_collision_with_list(self.player_sprite, self.coin_list)

        # Loop through each colliding sprite, remove it, and add to the score.
        for coin in hit_list:
            coin.remove_from_sprite_lists()
            self.score += 1

        if self.score == 1:
            test = 1


        if self.total_time <= 0:
            loss = 1
            #os.close()


def main():
    """ Main function """
    window = MyGame()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
test = 1